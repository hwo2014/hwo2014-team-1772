package hwo2014

import MasterAgent._
import CarPhysics.withinEpsilon

object MasterAgent {
  case class State(phase: String,
                   carId: CarId,
                   gameInfo: GameInfo,
                   track: TrackStructure,
                   carPhysics: CarPhysics,
                   turboAvailable: Option[TurboAvailable],
                   previousCarPositions: CarPositions,
                   carIdsOut: Set[CarId],
                   previousReaction: AgentReaction,
                   pendingSwitch: Option[SwitchLane]) {
    def isValid = (
      phase != null && carId != null && gameInfo != null &&
        track != null && carPhysics != null && turboAvailable != null &&
        previousCarPositions != null && previousReaction != null
    )
    def crashed: Boolean = carIdsOut(carId)
  }


  def emptyState: State =
    State("BEFORE_INIT",
          null,
          null,
          null,
          CarPhysics.empty,
          None,
          null,
          Set.empty,
          NOP,
          None)
}

class MasterAgent(exitOnCrash: Boolean) extends LoggerSupport {
  // TODO: HList would be nice here

  //val bendAngleAgent = new BendAngleAgent
  //val bendBreakAgent = new BendBreakAgent
  //val opponentAgent  = new OpponentAgent
  type State = MasterAgent.State
  def initialState: State = emptyState
  def yourCar(state: State, carId: CarId): State = state.copy(carId = carId)
  def gameInit(state: State, gameInfo: GameInfo): State = {
    logger.info(s"Cars: ${gameInfo.cars.map(_.id).mkString(", ")}")
    // TODO: calculate everything that is possible based on gameInfo
    state.copy(gameInfo = gameInfo,
               phase = Phases.GameInit,
               track = TrackStructure(gameInfo.track),
               turboAvailable = None,
               carIdsOut = Set.empty)
  }
  def gameStart(state: State): State = {
    logger.debug(s"${state.gameInfo.car(state.carId).dimensions.length} ${state.gameInfo.car(state.carId).dimensions.tailLength}")
    logger.debug(s"${state.gameInfo.track.pieces.mkString(",")}")
    state.copy(phase = Phases.GameStart,
               previousCarPositions = state.previousCarPositions.copy(gameTick = 0))
  }
  def gameEnd(state: State, results: List[CarResult]): State =
    state.copy(phase = Phases.GameEnd)
  def turboAvailable(state: State, turboAvailable: TurboAvailable): State =
    state.copy(turboAvailable = Some(turboAvailable).filter(_.factor > 1.0))
  def crash(state: State, carId: CarId): State = {
    val newCarsOut = state.carIdsOut + carId
    if (carId == state.carId) {
      logger.info(s"OUR CAR CRASHED (speed ${state.carPhysics.speed}, after tick ${state.previousCarPositions.gameTick})")
      if (exitOnCrash) { sys.exit(1) }
      val carStats = CarStatistics(state, state.previousCarPositions)
      state.copy(carPhysics = CarPhysics.updateOnCrash(carStats),
                 carIdsOut = newCarsOut)
    } else {
      logger.info(s"$carId CRASHED")
      state.copy(carIdsOut = newCarsOut)
    }
  }
  def dnf(state: State, carId: CarId): State = {
    state.copy(carIdsOut = state.carIdsOut + carId)
  }
  def spawn(state: State, carId: CarId): State = {
    logger.debug(s"BACK ON TRACK: $carId")
    state.copy(carIdsOut = state.carIdsOut - carId)
  }

  def drive(state: State, carPositions: CarPositions): (State, AgentReaction) = {
    if (carPositions.gameTick == -1) {
      val nextReaction = Throttle(1.0)
      val newState =
        state.copy(
          previousCarPositions = carPositions,
          previousReaction = nextReaction)
      (newState, nextReaction)
    } else if (!state.crashed) {
      assert(state.isValid)

      val carStats = CarStatistics(state, carPositions)

      val newCarPhysics = CarPhysics.updateOnDrive(state.carPhysics,
                                                   state.track,
                                                   state.carId,
                                                   state.previousCarPositions,
                                                   carPositions,
                                                   state.previousReaction)

      val (routeReaction, speedReactions) = time("calculate reactions", ifOverMillis = 10) {
        RouteAgent.react(carStats) ->
          // preference order, most preferred first
          List(
            TestPhysicsAgent.react(newCarPhysics),
            BendBreakAgent.react(state.track, state.pendingSwitch, newCarPhysics,
                                 carPositions(state.carId),
                                 state.gameInfo.car(state.carId)),
            TurboAgent.react(state.turboAvailable, carStats),
            DriftAgent.react(state.track, state.carPhysics, newCarPhysics,
                             carPositions(state.carId), state.previousReaction)
          )
      }

      val nextSpeedReaction: AgentReaction =
        speedReactions.find(_ != NOP).getOrElse(NOP)

      val nextReaction =
        routeReaction.flatMap { case (switch, distance) =>
          val mustSwitch = withinEpsilon(distance, newCarPhysics.speed)
          val canSwitch = nextSpeedReaction == NOP || nextSpeedReaction == state.previousReaction
          if (mustSwitch || canSwitch) {
            logger.info(s"Signal switch $switch")
            Some(switch)
          } else {
            None
          }
        }.getOrElse(nextSpeedReaction)

      def switchedLanes(oldPositions: CarPositions,
                        newPositions: CarPositions, carId: CarId): Boolean = {
        val newPosIndex = newPositions(carId).piece.index
        oldPositions(carId).piece.index != newPosIndex &&
          state.track.isSwitch(newPosIndex)
      }

      val usingTurbo = nextReaction.isInstanceOf[Turbo]

      val newState =
        state.copy(
          carPhysics = newCarPhysics,
          previousCarPositions = carPositions,
          turboAvailable =
            if (usingTurbo) None else state.turboAvailable,
          previousReaction = nextReaction,
          pendingSwitch = nextReaction match {
            case switch: SwitchLane => Some(switch)
            case _ =>
              if (switchedLanes(state.previousCarPositions, carPositions, state.carId))
                None
              else
                state.pendingSwitch
          })

      (newState, nextReaction)
    } else {
      (state, NOP)
    }
  }

}
