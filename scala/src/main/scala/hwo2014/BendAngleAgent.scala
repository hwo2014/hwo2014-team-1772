package hwo2014

import MasterAgent.State
import math.{max, min}

// TODO: remove this file, it is not used

/** A simple PD-controller that tries to maintain a maximum angle in
  * bends
object BendAngleAgent extends LoggerSupport {
  val maxAngle = 60.0
  val maxAngleDiff = 3.0
  val maxThrottle = 1.0

  def react(track: TrackStructure, 
            previousCarPosition: CarPosition,
            newCarPosition: CarPosition): AgentReaction = {
    val carAbsoluteAngle = newCarPosition.angle
    val previousAbsoluteAngle = previousCarPosition.angle
    val bendDirection = track.bendDirection(newCarPosition.piece.index)
    val throttle =
      if (bendDirection == 0) {
        maxThrottle
      } else {
        val (carAngle, prevCarAngle) =
          (bendDirection * carAbsoluteAngle,
           bendDirection * previousAbsoluteAngle)

        /* TODO:
         * - need to detect if the angle is out from the bend or towards
         *   the center.  If there are two consecutive bends to opposite
         *   directions then the car can actually enter the second bend
         *   nose pointing out. In this case we don't want to hit the
         *   throttle before the car has turned nose in (otherwise it will
         *   just shoot out).
         *
         * - instead of trying to limit the angle, we actually want the
         *   angle. I.e. we want to drift. As such, driving these cars is
         *   very different from driving on public roads. The simple 'P'
         *   tries to limit drift, so it cannot be used. Instead we need a
         *   controller that hits the gas pendal when the angle is above
         *   45 (so that we have a sufficient inward component).
         */

        // a simple P (proportional) controller.
        val throttleP =
          max(1 - (carAngle / maxAngle), 0)

        // a simple D (differential) controller.
        val throttleD = max(1 - ((carAngle - prevCarAngle) / maxAngleDiff), 0)

        // a simple combination of PD
        val throttlePD = throttleP * throttleD

        // logger.debug(f"Throttle at $throttleP%.2f * $throttleD%.2f = $throttlePD%.2f, angle $carAngle%2.0f")

        if (carAngle > 25) {
          logger.debug(f"carAngle $carAngle%2.0f")
        }

        maxThrottle * throttlePD
      }

    Throttle(throttle)
  }
}
  */
