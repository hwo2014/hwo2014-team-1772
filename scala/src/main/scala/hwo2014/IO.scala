package hwo2014

import java.io.{File, FileWriter, InputStreamReader, BufferedReader, OutputStreamWriter, PrintWriter}
import java.net.Socket
import java.util.concurrent.TimeoutException
import org.json4s.jackson.JsonMethods._
import org.json4s.{Extraction, JValue, JNothing}
import scala.annotation.tailrec
import scala.concurrent.{Future, Await}

class IO[T](host: String, port: Int, agent: MasterAgent, logToFile: Boolean)
    extends LoggerSupport {
  import JsonSupport.formats
  import AlphaBot._
  import scala.concurrent.ExecutionContext.Implicits.global

  val fileLogger: Option[((String => Unit), File)] = {
    import org.joda.time.DateTime
    import org.joda.time.format.DateTimeFormat
    if (logToFile) {
      val f = File.createTempFile("hwo2014_", ".log")
      val fw = new FileWriter(f)
      val fmt = DateTimeFormat.forPattern("HH:mm:ss.SSS")
      val now = DateTime.now.toString(fmt)
      val writer = (message: String) => {
        fw.write(s"$now \t $message\n")
        fw.flush
      }
      fw.write(s"Start logging @ ${DateTime.now}\n")
      Some(writer, f)
    } else {
      None
    }
  }

  warmUpJson()

  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, CharSet))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, CharSet))
  logger.info(s"Initialized connection to $host:$port")
  fileLogger.foreach { case (writer, file) =>
    logger.info(s"Writing log to $file ...")
  }

  def sendReaction(gameTick: Long, reaction: AgentReaction): Unit = {
    if (gameTick % 10 == 0) {
      logger.debug(s"tick: ${gameTick}, reaction: $reaction")
    }

    // don't send anything if tick < 0
    if (gameTick >= 0) {
      val msg = MessageWrapper(Some(gameTick), reaction)
      sendJson(Extraction.decompose(msg))
    }
  }

  def sendJson(json: JValue): Unit = {
    val js = compact(json)
    // logger.debug(s"SEND: $js")
    fileLogger.foreach(_._1(s"TO SERVER: $js"))
    writer.println(js)
    writer.flush
  }

  private type PendingReaction = Option[Future[(agent.State, AgentReaction)]]

  @tailrec
  private def handleNextEvent(oldGameState: agent.State,
                              pendingReaction: PendingReaction): Unit = {
    import hwo2014.{JsonSupport => JS}

    val line = reader.readLine()
    if (line != null) {
      fileLogger.foreach(_._1(line))
      val json = parse(line)
      val msgType = (json \ "msgType").extractOrElse[String]("NO_MSG_TYPE")

      if (msgType == "carPositions") {
        val carPositions = JS.parseCarPositions(json)
        val (newGameState, newPendingReaction) =
          react(oldGameState, carPositions, pendingReaction)
        handleNextEvent(newGameState, newPendingReaction)
      } else {
        val newGameState: agent.State = msgType match {
          case "crash" =>
            agent.crash(oldGameState, JS.dataAs[CarId](json))
          case "dnf" =>
            logger.info(s"DNF: $line")
            agent.dnf(oldGameState, (json \ "data" \ "car").extract[CarId])
          case "spawn" =>
            agent.spawn(oldGameState, JS.dataAs[CarId](json))
          case "yourCar" =>
            val carId = JS.dataAs[CarId](json)
            logger.info(s"$msgType: $carId")
            agent.yourCar(oldGameState, carId)
          case Phases.GameInit =>
            logger.info(s"$msgType")
            time("gameInit") {
              agent.gameInit(oldGameState, gameInfo = time("JS.parseGameInfo")(JS.parseGameInfo(json)))
            }
          case Phases.GameStart =>
            logger.info(s"$msgType")
            // hack, but acroding to the spec, we need to react to gameStart
            sendReaction(0, Throttle(1.0)) 
            agent.gameStart(oldGameState)
          case Phases.GameEnd =>
            val GameResults(results) = JS.parseGameResults(json)
            logger.info(s"Game ended: $results")
            agent.gameEnd(oldGameState, results)
          case "turboAvailable" =>
            agent.turboAvailable(oldGameState, JS.parseTurboAvailable(json))
          case x =>
            logger.debug(s"Received: $line")
            oldGameState
        }

        handleNextEvent(newGameState, pendingReaction)
      }
    } else {
      logger.warn("Got EOF from server")
      fileLogger.foreach { case (_,file) =>
        logger.info(s"Log written to $file")
      }
    }
  }

  //val speedWriter = new java.io.FileWriter(new java.io.File("/tmp/speed.tsv"))
  private def react(oldGameState: agent.State, carPositions: CarPositions,
            pendingReaction: PendingReaction):
      (agent.State, PendingReaction) = {
    /*
    if (oldGameState.isValid) {
       speedWriter.write(List(
         oldGameState.previousCarPositions.gameTick.toString,
         oldGameState.carPhysics.speed,
         oldGameState.carPhysics.speedIsReliable
       ).mkString("\t") + "\n")
       speedWriter.flush()
    }
    */
    val agentFuture =
      pendingReaction getOrElse Future(agent.drive(oldGameState, carPositions))

    // Try to wait for the agent, but if it fails to respond in
    // 15 ms, send a ping to server and process next server message.
    // However, when the agent is ready, its original reaction will
    // be used.
    try {
      import scala.concurrent.duration._

      val (newGameState, reaction) = Await.result(agentFuture, 15.millis)

      sendReaction(carPositions.gameTick, reaction)

      newGameState -> None
    } catch {
      case e: TimeoutException =>
        val fallbackReaction = if(carPositions.gameTick < 3) Throttle(1.0) else Ping
        logger.warn(s"Agent did not reply in time, sending $fallbackReaction")
        sendReaction(carPositions.gameTick, fallbackReaction)
        oldGameState -> Some(agentFuture)
    }
  }

  def warmUpJson(): Unit = {
    // decompose will take 1s on the first call, so we need
    // to warm it up
    Extraction.decompose(MessageWrapper(None, Throttle(1.0)))
  }

  def start(joinMessage: JValue): Unit = {
    logger.info(s"Send join request:\n${pretty(joinMessage)}")
    sendJson(joinMessage)
    handleNextEvent(agent.initialState, None)
  }
}
