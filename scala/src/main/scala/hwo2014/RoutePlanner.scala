package hwo2014

import annotation.tailrec

trait RoutePlanner {
  def planRoute(track: Track): List[Int]
}

@deprecated("use DynamicProgrammingPlanner", "2014-04-21")
object SimpleRoutePlanner extends RoutePlanner {

  // static route planner (currently doesn't consider other cars).
  // TODO: plan where to switch lanes.
  // TODO: verify that the plan is optimal.
  // ==> see DynamicProgrammingPlanner
  override def planRoute(track: Track): List[Int] = {
    val lanes = track.lanes.map( l => l.index )
    val (minLaneIdx, maxLaneIdx) = (lanes.min, lanes.max) // assume min is always the inner and max the outer lane
    val pieces = track.pieces

    // plan optimal lanes for bends
    val pass1 = pieces.map {
      case Straight(_, switch) => lanes.toSet
      case Bend(_, angle, switch) => if (angle > 0) Set(minLaneIdx) else Set(maxLaneIdx)
    }.toList

    // plan optimal lanes for the whole track
    planRoute(pass1, (0 to pass1.size-1).toList zip pass1, List())
  }

  private def planRoute(bendPlan: List[Set[Int]], remaining: List[(Int, Set[Int])], acc: List[Int]): List[Int] = {
    def nextFixedLane(bendPlan: List[Set[Int]], idx: Int, r: Int): Int = {
      if(bendPlan(idx).size == 1)
        bendPlan(idx).head
      else
        nextFixedLane(bendPlan, (idx + 1) % (bendPlan.size-1), r+1)
    }

    if(remaining.isEmpty)
      acc
    else {
      planRoute(bendPlan, remaining.tail, acc :+ nextFixedLane(bendPlan, remaining.head._1, 0))
    }
  }
}

object DynamicProgrammingPlanner {
  // TODO: allow specifying "obstacles"

  def calculatePolicy(track: Track): Array[List[AgentReaction]] = {

    def truncate(n: Double, p: Int) = {
      val s = math.pow(10, p)
      math.rint(n * s) / s
    }

    // calculate value per track piece based on static track properties (i.e. shape)
    def calculateTrackValues = {
      @tailrec def calculateTrackValuesAcc(pieces: Array[TrackPiece],
                                           cumulativeLaneValues: List[Double],
                                           acc: List[List[Double]]):
      List[List[Double]] = {
        if(pieces.isEmpty) {
          acc
        } else {
          val pieceValues: List[Double] = pieces.head match {
            case Straight(length, switch) =>  List.fill(track.lanes.length)(length)
            case Bend(radius: Double, angle: Double, switch: Boolean) => {
              val l = track.lanes.map(p => (radius + p.distanceFromCenter) * math.Pi * math.abs(angle) / 180).toList
              if (angle < 0) l
              else l.reverse
            }
          }
          val adjustedCumulativeValues = if (pieces.head.switch)
            (0 until cumulativeLaneValues.length).map(i => minimumAdjacentLaneValue(i, cumulativeLaneValues)._1).toList
          else
            cumulativeLaneValues
          val newTotalLaneValues = (adjustedCumulativeValues zip pieceValues) map (v => truncate(v._1 + v._2, 2))
          calculateTrackValuesAcc(pieces.tail, newTotalLaneValues , acc :+ newTotalLaneValues)
        }
      }

      val r = calculateTrackValuesAcc(track.pieces.reverse, List.fill(track.lanes.length)(0.0),
        List()).reverse
//      r.zipWithIndex.foreach(z => println(s"v: ${track.id}, ${z._2}, ${z} # ${track.pieces(z._2)}}"))
      r
    }

    def minimumAdjacentLaneValue(lane: Int, laneValues: List[Double]): (Double, Int) = {
      val start = if(lane == 0) 0 else lane - 1
      val end = if(lane == laneValues.length - 1) lane else lane + 1
      val r = laneValues.zipWithIndex.slice(start, end+1).minBy( v => v._1 )
      r
    }

    // determine optimal policy based on calculated track values
    def planOptimalPolicy(trackValues: List[List[Double]]): Array[List[AgentReaction]]= {
      val laneCount = track.lanes.length
      val plan = (0 until track.pieces.length).map( i => {

        // calculate optimal action (i.e. switch lanes)
        val nxt1 = (i + 1) % track.pieces.length
        val nxt2 = (i + 2) % track.pieces.length
        if(track.pieces(nxt1).switch) {
          (0 until laneCount).map( l => {
            val m1 = minimumAdjacentLaneValue(l, trackValues(nxt2))
            // apply consistent policy for ties (i.e. inner track wins)
            val m = trackValues(nxt2).zipWithIndex.filter(z => z._1 == m1._1).reverse.head
            if(m._2 > l) SwitchLaneRight
            else if (m._2 < l) SwitchLaneLeft
            else NOP
          }).toList
        } else {
          List.fill(laneCount)(NOP)
        }
      })
      plan.toArray
    }

    val policy = planOptimalPolicy(calculateTrackValues)
    //policy.zipWithIndex.foreach(i=> println(s"${i._1}: ${track.pieces(i._2)}"))

    policy
  }

}

