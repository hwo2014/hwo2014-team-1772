package hwo2014

import math.{Pi,abs,min,max,log,pow,sqrt,acos}

object BendBreakAgent extends LoggerSupport {
  def react(track: TrackStructure, pendingSwitch: Option[SwitchLane], carPhysics: CarPhysics,
            carPosition: CarPosition, car: Car): AgentReaction = {
    val currentIndex = carPosition.piece.index
    val currentLaneIndex = carPosition.piece.lane.endIndex
    val currentSpeed = carPhysics.speed
    val drag = carPhysics.drag

    val distanceToBend = track.carDistanceToBend(carPosition)
    val bendRadius = {
      val relevantLaneIndex =
        pendingSwitch.filter(_ => track.carDistanceToSwitch(carPosition) <= distanceToBend).
          map(track.resultingLaneIndex(currentLaneIndex, _)).
          getOrElse(currentLaneIndex)
      track.bendRadius(track.nextBendIndices(currentIndex), relevantLaneIndex)
    }

    if (distanceToBend == Double.PositiveInfinity) {
      // nothing to do, it's an uniform track
      return NOP
    }

    //val intoBendAngle = acos((bendRadius - car.dimensions.tailLength) / bendRadius)
    //val intoBendDistance = intoBendAngle * bendRadius

    // calculate maximum speed in the bend
    val maxSpeed = sqrt(carPhysics.maxBendForce * bendRadius)
    // calculate time it takes to break to maxSpeed from currentSpeed
    // solved breakTicks from: maxSpeed = currentSpeed * power(drag, breakTicks)
    val breakTicks = log(currentSpeed / maxSpeed) / -log(drag)
    // calculate distance it takes to break to maxSpeed from startSpeed
    // solved sum: distance = startSpeed * (1 to breakTicks).map(power(newDrag, breakTicks)).sum
    val breakDistance =
      if (drag >= 1.0) Double.PositiveInfinity
      else currentSpeed * (drag * (pow(drag, breakTicks) - 1) / (drag - 1))

    // decision time
    val break = currentSpeed > maxSpeed && distanceToBend <= (breakDistance + (currentSpeed / 2))

    if (break) {
      val throttle = carPhysics.estimateThrottle(currentSpeed, maxSpeed)
      // logger.debug(f"Slowing down with throttle $throttle%.2f to $currentSpeed%.2f > $maxSpeed%.2f in $breakDistance%.1f (${car.dimensions.tailLength} / $bendRadius)")
      Throttle(throttle)
    } else NOP
  }
    
}


