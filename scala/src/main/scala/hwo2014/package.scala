import com.typesafe.scalalogging.slf4j.Logger
import org.json4s.{Extraction, JValue, JString, JNothing}

package object hwo2014 {
  object Phases {
    val BeforeInit = "BEFORE_INIT"
    val GameInit = "gameInit"
    val GameStart = "gameStart"
    val GameEnd = "gameEnd"
  }

  /* These are the messages that the bot will send to the server */
  sealed trait BotMessage
  /* A subset of them are allowed as reactions from the agents */
  sealed trait AgentReaction extends BotMessage

  case class Join(name: String, key: String) extends BotMessage
  case class CarId(name: String, color: String)

  // gameInit
  case class GameInfo(gameId: String, track: Track, cars: List[Car], raceSession: RaceSession) {
    def findCar(carId: CarId): Option[Car] = cars.find(_.id == carId)
    def car(carId: CarId): Car = findCar(carId).get
  }
  case class Track(id: String, pieces: Array[TrackPiece], lanes: Array[TrackLane], startingPoint: StartingPoint)
  sealed trait TrackPiece { def switch: Boolean; def isBend: Boolean }
  case class Straight(length: Double, switch: Boolean) extends TrackPiece {
    val isBend = false
  }
  case class Bend(radius: Double, angle: Double, switch: Boolean) extends TrackPiece {
    val length = radius * math.Pi * math.abs(angle) / 180 // center of the track
    val isBend = true
  }
  case class TrackLane(index: Int, distanceFromCenter: Double)
  case class StartingPoint(x: Double, y: Double, angle: Double)
  case class CarDimensions(length: Double, width: Double, guideFlagPosition: Double) {
    val tailLength = length - guideFlagPosition
  }
  sealed trait RaceSession { def isQualifyingSession: Boolean }
  case class QualifyingSession(durationMs: Long) extends RaceSession { val isQualifyingSession = true }
  case class MainRaceSession(laps: Int, maxLapTimeMs: Long, quickRace: Boolean) extends RaceSession { val isQualifyingSession = false }
  case class Car(id: CarId, dimensions: CarDimensions)

  // carPositions
  case class CarPositions(gameTick: Long, cars: List[CarPosition]) {
    def findCar(carId: CarId): Option[CarPosition] = cars.find(_.id == carId)
    def apply(carId: CarId): CarPosition = findCar(carId).get
  }
  case class CarPosition(id: CarId, angle: Double, piece: PiecePosition, lap: Int)
  case class PiecePosition(index: Int, distance: Double, lane: LanePosition)
  case class LanePosition(startIndex: Int, endIndex: Int)
  object CarPosition {
    val empty = CarPosition(CarId("", ""), 0.0, PiecePosition(0, 0.0, LanePosition(0, 0)), 0)
  }

  // turboAvailable
  case class TurboAvailable(durationMillis: Long, durationTicks: Long, factor: Double)

  // gameEnd
  case class GameResults(results: List[CarResult])
  case class CarResult(id: CarId, result: Option[TotalResult], bestLap: Option[BestLap])
  case class TotalResult(laps: Int, ticks: Long, millis: Long)
  case class BestLap(lap: Int, ticks: Long, millis: Long)

  // messages to server
  case class Throttle1(throttle: Double) extends AgentReaction {
    override def toString = f"Throttle($throttle%.2f)"
  }

  object Throttle {
    def apply(throttle: Double) =
      if (throttle == Double.PositiveInfinity) Throttle1(1.0)
      else if (throttle == Double.NaN) Throttle1(0.5)
      else if (throttle == Double.NegativeInfinity) Throttle1(0.0)
      else Throttle1(math.min(math.max(throttle, 0.0), 1.0))

    def unapply(throttle: Throttle1) = Some(throttle.throttle)
  }

  sealed trait SwitchLane extends AgentReaction
  case object SwitchLaneRight extends SwitchLane { override def toString = "SwitchLaneRight" }
  case object SwitchLaneLeft extends SwitchLane { override def toString = "SwitchLaneLeft" }
  case class Turbo(message: String) extends AgentReaction
  object Turbo {
    val OneLiners = List("It's turbo time, baby!", "Can't touch this!", "I have the need.. NEED FOR SPEED!",
                         "VRUUUM", "Look ma, no hands!")

    def withRandomMessage: Turbo = {
      val message = OneLiners((math.random * OneLiners.length).toInt)
      Turbo(message)
    }
  }
  case object Ping extends AgentReaction { override def toString = "Ping" }
  // A ping can be used as a No-Operation message
  val NOP = Ping

  case class MessageWrapper(msgType: String, data: JValue, gameTick: Option[Long])
  object MessageWrapper {
    def apply(gameTick: Option[Long], data: AgentReaction): MessageWrapper = {
      implicit val formats = JsonSupport.formats
      data match {
        case Throttle1(throttle) =>
          MessageWrapper("throttle", Extraction.decompose(math.max(0.0, math.min(1.0, throttle))), gameTick)

        case SwitchLaneRight =>
          MessageWrapper("switchLane", JString("Right"), gameTick)

        case SwitchLaneLeft =>
          MessageWrapper("switchLane", JString("Left"), gameTick)

        case Ping =>
          MessageWrapper("ping", JNothing, gameTick)

        case Turbo(message) =>
          MessageWrapper("turbo", JString(message), gameTick)
      }
    }
  }

  def time[T](message: String, ifOverMillis: Long = -1)(action: => T)(implicit logger: Logger): T = {
    val start = System.currentTimeMillis
    val value = action
    val duration = System.currentTimeMillis - start
    if (duration > ifOverMillis) {
      logger.debug(s"$message took $duration ms")
    }
    value
  }

  /** Distance travelled since start of race */
  @deprecated("2014-04-20", "Don't use! Doesn't count effect of lanes")
  def position(pieceStartPositions: Array[Double], carPosition: CarPosition): Double = {
    pieceStartPositions(carPosition.piece.index) +
      carPosition.piece.distance +
      (carPosition.lap * pieceStartPositions.last)
  }

  object vector {
    import math.{sqrt, pow}
    // plane equation

    case class Vector(x: Double, y: Double, z: Double) {
      def -(other: Vector) = substract(this, other)
      def +(other: Vector) = add(this, other)
      def x(other: Vector) = crossProduct(this, other)
    }

    // ax + by + cz + d == 0
    case class Plane(a: Double, b: Double, c: Double, d: Double)

    def crossProduct(v1: Vector, v2: Vector): Vector = 
      Vector(
        v1.y*v2.z - v1.z*v2.y, // x=yzzy
        v1.z*v2.x - v1.x*v2.z,
        v1.x*v2.y - v1.y*v2.x
      )

    def substract(v1: Vector, v2: Vector): Vector =
      Vector(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z)

    def add(v1: Vector, v2: Vector): Vector =
      Vector(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z)

    def plane(v1: Vector, v2: Vector, v3: Vector): Plane = {
      val v1v2 = v2 - v1
      val v1v3 = v3 - v1
      val normal = v1v2 x v1v3
      val d = -((normal.x * v1.x) + (normal.y * v1.y) + (normal.z * v1.z))
      Plane(normal.x, normal.y, normal.z, d)
    }

    def solveZ(plane: Plane, x: Double, y: Double) = 
      -(plane.d + (plane.a * x) + (plane.b * y)) / plane.c

    def solveY(plane: Plane, x: Double, z: Double) =
      -(plane.d + (plane.a * x) + (plane.c * z)) / plane.b

  }
}
