package hwo2014

import org.json4s._
import org.json4s.jackson.JsonMethods.pretty
import scala.util.control.NonFatal
import scala.util.Try

object JsonSupport extends LoggerSupport {

  implicit val formats = new DefaultFormats{} + TrackPieceSerializer +
    StartingPointSerializer + CarPositionSerializer + RaceSessionSerializer

  def dataAs[T: Manifest](json: JValue) = (json \ "data").extract[T]

  def parseGameInfo(json: JValue): GameInfo = {
    val race = json \ "data" \ "race"
    val track = (race \ "track").extract[Track]
    val cars = (race \ "cars").extract[List[Car]]
    val raceSession = (race \ "raceSession").extract[RaceSession]
    GameInfo(gameId(json), track, cars, raceSession)
  }

  def parseCarPositions(json: JValue): CarPositions = {
    val cars = dataAs[List[CarPosition]](json)
    CarPositions(gameTick(json), cars)
  }

  def parseGameResults(json: JValue): GameResults = {
    def carId(parent: JValue) = (parent \ "car").extract[CarId]
    try {
      val results = (json \ "data" \ "results").children.map { result =>
        carId(result) -> Try((result \ "result").extract[TotalResult]).toOption
      }.toMap

      val bestLaps = (json \ "data" \ "bestLaps").children.map { bestLap =>
        carId(bestLap) -> Try((bestLap \ "result").extract[BestLap]).toOption
      }.toMap

      GameResults(results.map { case (car, r) =>
        CarResult(car, results(car), bestLaps(car))
      }.toList.sortBy(_.result.map(_.millis).getOrElse(Long.MaxValue)))

    } catch {
      case e: Exception =>
        logger.error(s"Error parsing GameResults from:\n ${pretty(json)}", e)
        GameResults(Nil)
    }
  }

  def parseTurboAvailable(json: JValue): TurboAvailable = {
    val data = json \ "data"
    val millis = (data \ "turboDurationMilliseconds").extract[Double].toLong
    val ticks = (data \ "turboDurationTicks").extract[Long]
    val factor = (data \ "turboFactor").extract[Double]
    TurboAvailable(millis, ticks, factor)
  }

  private def gameId(json: JValue) =
    (json \ "gameId").extractOrElse[String]("UNKNOWN")

  private def gameTick(json: JValue): Long =
    (json \ "gameTick").extractOrElse[Long] {
      // logger.warn(s"Got no gameTick: $json")
      -1L
    }

  object TrackPieceSerializer extends CustomSerializer[TrackPiece]( _ =>
    ({
      case o: JObject =>
        val switch = (o \ "switch").extractOrElse[Boolean](false)
        (o \ "length").extractOpt[Double] match {
          case Some(length) =>
            Straight(length, switch)
          case None =>
            Bend((o \ "radius").extract[Double], (o \ "angle").extract[Double], switch)
        }
      case JNull => null
    }, PartialFunction.empty)
  )

  object StartingPointSerializer extends CustomSerializer[StartingPoint]( _ =>
    ({
      case o: JObject =>
        val pos = (o \ "position")
        StartingPoint((pos \ "x").extract[Double], (pos \ "y").extract[Double], (o \ "angle").extract[Double])
      case JNull => null
    }, PartialFunction.empty)
  )

  object RaceSessionSerializer extends CustomSerializer[RaceSession]( _ =>
    ({
      case o: JObject =>
        try {
          (o \ "durationMs").extractOpt[Long].
            map(QualifyingSession(_)).
            getOrElse(o.extract[MainRaceSession])
        } catch {
          case NonFatal(e) =>
            logger.error(s"Error parsing RaceSession from\n: ${pretty(o)}")
            null // because we probably aren't reading RaceSession anyway...
        }
      case JNull => null
    }, PartialFunction.empty)
  )

  object CarPositionSerializer extends CustomSerializer[CarPosition]( _ =>
    ({
      case o: JObject =>
        val id = (o \ "id").extract[CarId]
        val angle = (o \ "angle").extract[Double]
        val pos = (o \ "piecePosition")
        val lane = (pos \ "lane")
        val piecePosition = PiecePosition(
          (pos \ "pieceIndex").extract[Int],
          (pos \ "inPieceDistance").extract[Double],
          LanePosition((lane \ "startLaneIndex").extract[Int], (lane \ "endLaneIndex").extract[Int])
        )
        CarPosition(id, angle, piecePosition, (pos \ "lap").extract[Int])
      case JNull => null
    }, PartialFunction.empty)
  )

}
