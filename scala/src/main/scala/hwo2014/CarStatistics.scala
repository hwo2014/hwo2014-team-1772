package hwo2014

import MasterAgent.State
import CarStatistics._

case class OpponentCarInfo(carPos: CarPosition, distance: Double, isBehindUs: Boolean,
                           isSameLaneAsUs: Boolean, speed: Option[Double]) {
  def isFrontOfUs = !isBehindUs
  def carId = carPos.id
}

case class CarStatistics(state: State, carPositions: CarPositions) extends LoggerSupport {
  def cars = state.gameInfo.cars
  def carId = state.carId
  def gameTick = carPositions.gameTick
  def carDimensions = state.gameInfo.cars.find(_.id == carId).get.dimensions
  lazy val opponentCarInfos = {
    val carInfos = calculateOpponentCarInfos(state, carPositions)
    if (carPositions.gameTick % 10 == 0) {
      logger.info(
        s"""
           |On tick ${carPositions.gameTick} opponents are:
           |${carInfos.mkString("\n")}
         """.stripMargin.trim)
    }
    carInfos
  }
}

object CarStatistics {
  import CarPhysics.{distanceDelta, speed}

  def calculateOpponentCarInfos(state: State, carPositions: CarPositions): List[OpponentCarInfo] = {
    val carId = state.carId
    val ourCarPos = carPositions(carId)
    val ourPiece = ourCarPos.piece
    val ourLaneEndIndex = ourPiece.lane.endIndex
    carPositions.cars.filterNot(pos => pos.id == carId || state.carIdsOut(pos.id)).map { pos =>
      val otherPiece = pos.piece
      val pieceDist = state.track.distanceToPiece(ourPiece.index, otherPiece.index, ourLaneEndIndex)
      val dist = math.abs(pieceDist - ourPiece.distance + otherPiece.distance) // TODO: How about CarDimensions
      val behindUs =
        if (ourPiece.index == otherPiece.index) ourPiece.distance > otherPiece.distance
        else {
          val pieceCount = state.track.pieces.length
          val ourTotalIndex = ourPiece.index + pieceCount*(ourCarPos.lap - 1)
          val otherTotalIndex = otherPiece.index + pieceCount*(pos.lap - 1)
          val totalPieceDiff = ourTotalIndex - otherTotalIndex
          if (totalPieceDiff > 0 && totalPieceDiff < pieceCount/2)
            true
          else
            false
        }
      val carSpeed: Option[Double] =
        if (state.previousCarPositions != null) {
          distanceDelta(state.previousCarPositions(pos.id),
                        carPositions(pos.id), state.track).flatMap { dd =>
            speed(dd, state.previousCarPositions.gameTick, carPositions.gameTick)
          }
        } else {
          None
        }
      val sameLane = pos.piece.lane.endIndex == ourPiece.lane.endIndex
      OpponentCarInfo(pos, dist, behindUs, sameLane, carSpeed)
    }
  }

}


