package hwo2014

import math.{abs, min, max, pow, log}
import annotation.tailrec

class TrackStructure(track: Track,
                     val nextBendIndices: Array[Int],
                     val nextSwitchIndices: Array[Int],
                     // NOTE: One 'bend' is seen as a sequence of pieces with identical radius
                     val bendStartIndices: Array[Int]) extends LoggerSupport {

  type TurboResult = List[(Int, Int)]

  private val turboLocations: TurboResult = TrackStructure.calculateTurboBoostLocations(track.pieces, false)
  private val turboLocationsForLastLap: TurboResult = TrackStructure.calculateTurboBoostLocations(track.pieces, true)

  def turboBoostLocations(lastLap: Boolean): TurboResult =
    if (lastLap) turboLocationsForLastLap
    else turboLocations

  /* Given any index on a bend, returns the index where the bend ends.
   * Return -1 if not in a bend.*/
  def bendEndIndex(bendIndex: Int): Int = {
    @tailrec def impl(index: Int, start: Int): Int = {
      if (bendStartIndices(index) == start)
        impl((index + 1) % bendStartIndices.length, start)
      else index
    }
    
    val start = bendStartIndices(bendIndex)
    if (start == -1) -1 else impl(bendIndex, start)
  }

  /** Returns -1 for left, 0 for straight, 1 for right */
  def bendDirection(index: Int): Int = track.pieces(index) match {
    case Bend(_, angle, _) => if (angle > 0) 1 else -1
    case _ => 0
  }

  def lanes = track.lanes

  def pieces = track.pieces

  def isBend(index: Int): Boolean = bendDirection(index) != 0

  def isSwitch(index: Int): Boolean = track.pieces(index).switch

  def bendRadius(index: Int, laneIndex: Int) = {
    val pieces = track.pieces
    val laneOffset = track.lanes(laneIndex).distanceFromCenter
    pieces(index) match {
      case Straight(length, _) => Double.PositiveInfinity 
      case Bend(radius, angle, _) if angle > 0 => (radius - laneOffset)
      case Bend(radius, angle, _) => (radius + laneOffset)
    }
  }

  def pieceLength(index: Int, laneIndex: Int) = {
    val pieces = track.pieces
    pieces(index) match {
      case Straight(length, _) =>
        length
      case Bend(_, angle, _) =>
        bendRadius(index, laneIndex) * math.Pi * abs(angle) / 180
    }
  }

  /**
    * Distance from the start of one piece to the other using the given lane.
    *
    * Remember to substract carPosition.piece.distance if you want the
    * accurate distance.
    */
  def distanceToPiece(fromIndex: Int, toIndex: Int, laneIndex: Int): Double = {
    val pieces = track.pieces

    val piecesLeft =
      if (toIndex >= fromIndex) toIndex - fromIndex - 1
      else pieces.size + toIndex - fromIndex - 1

    (0 to piecesLeft).map { i =>
      pieceLength((fromIndex + i) % pieces.length, laneIndex)
    }.sum
  }

  def carDistanceToPiece(carPos: CarPosition, toIndex: Int): Double = (
    distanceToPiece(carPos.piece.index, toIndex, carPos.piece.lane.endIndex) -
      carPos.piece.distance
  )

  /**
    * Distance from the start of the given piece to the next bend
    * using the given lane.
    *
    * Remember to substract carPosition.piece.distance if you want the
    * accurate distance (or use `carDistanceToBend`)
    */
  def distanceToBend(fromIndex: Int, laneIndex: Int): Double =
    distanceTo(nextBendIndices, fromIndex, laneIndex)

  def carDistanceToBend(carPos: CarPosition): Double = (
    distanceToBend(carPos.piece.index, carPos.piece.lane.endIndex) -
      carPos.piece.distance
  )

  def distanceToSwitch(fromIndex: Int, laneIndex: Int): Double =
    distanceTo(nextSwitchIndices, fromIndex, laneIndex)

  def carDistanceToSwitch(carPos: CarPosition): Double = (
    distanceToSwitch(carPos.piece.index, carPos.piece.lane.endIndex) -
      carPos.piece.distance
  )

  private def distanceTo(targetIndices: Array[Int], fromIndex: Int, laneIndex: Int): Double = {
    val i = targetIndices(fromIndex)
    if (i == -1) Double.PositiveInfinity
    else distanceToPiece(fromIndex, i, laneIndex)
  }

  val optimalRouteReactions: Array[List[AgentReaction]] = time("optimalRouteReactions") {
    DynamicProgrammingPlanner.calculatePolicy(track)
  }(TrackStructure.logger)

  def switchLaneReaction(carPos: CarPosition): Option[(SwitchLane, Double)] =
    switchLaneReactionByPieceAndLaneIndex(carPos.piece.index, carPos.piece.lane.endIndex, carPos.piece.distance)

  def switchLaneReactionByPieceAndLaneIndex(pieceIndex: Int, laneIndex: Int, pieceDistance: Double = 0): Option[(SwitchLane, Double)] = {
    val switch = nextSwitchIndices(pieceIndex)
    val beforeSwitch = if (switch == 0) track.pieces.length - 1 else switch - 1
    optimalRouteReactions(beforeSwitch)(laneIndex) match {
      case NOP => None
      case switchReaction: SwitchLane =>
        val decisionDistance = distanceToSwitch(pieceIndex, laneIndex) - pieceDistance
        Some(switchReaction -> decisionDistance)
      case x =>
        logger.error(s"Unexpected route reaction: $x")
        None
    }
  }

  def resultingLaneIndex(currentLaneIndex: Int, switch: SwitchLane): Int = {
    def findByOffset(offset: Int) =
      track.lanes.find(_.index == currentLaneIndex+offset).
        map(_.index).getOrElse(currentLaneIndex)
    switch match {
      case SwitchLaneLeft => findByOffset(-1)
      case SwitchLaneRight => findByOffset(1)
    }
  }

}

object TrackStructure extends LoggerSupport {
  def apply(track: Track): TrackStructure = {
    val nbi = nextBendIndices(track.pieces)
    new TrackStructure(track,
                       nbi,
                       nextSwitchIndices(track.pieces),
                       bendStartIndices(track.pieces, nbi))
  }

  def bendStartIndices(pieces: Array[TrackPiece],
                       nextBendIndices: Array[Int]): Array[Int] =
    if (nextBendIndices(0) == -1) nextBendIndices else {
      val bendStartIndices = Array.ofDim[Int](pieces.length)
      val starts = nextBendIndices.toSet - (-1)
      val last = starts.max
      def impl(i: Int, starts: Set[Int]): Unit =
        if (i >= 0) {
          bendStartIndices(i) =
            if (!pieces(i).isBend) -1
            else if (starts.nonEmpty) starts.max
            else last

          impl(i - 1, starts - i)
        }
      impl(pieces.length - 1, starts)

      bendStartIndices
    }

  def nextBendIndices(pieces: Array[TrackPiece]): Array[Int] = {
    // yeah, yeah.. all imperative, but fast

    val nextBendIndices = Array.ofDim[Int](pieces.length)

    // find first bend index
    val firstBendIndex = (pieces.last, pieces.head) match {
      case (b1: Bend, b2: Bend) if (b1.angle == b2.angle) =>
        // the start is in a bend, search bend after first non-bend
        val startIndex = pieces.indexWhere(!_.isBend)
        pieces.indexWhere(_.isBend, startIndex)
      case _ =>
        pieces.indexWhere(_.isBend)
    }

    // from last to first, find out the next bend
    nextBendIndices(nextBendIndices.length - 1) = firstBendIndex

    for (i <- pieces.length - 2 to 0 by -1) {
      val index = pieces(i) match {
        case bend: Bend => pieces.indexWhere(
          {
            case other: Bend => bend.angle != other.angle
            case _ => false
          }, i + 1)

        case _ => pieces.indexWhere(_.isBend, i)
      }

      assert(index == -1 || index > i, s"$index > $i")

      nextBendIndices(i) = if (index == -1) firstBendIndex else index
    }
  
    nextBendIndices
  }

  // determine locations for applying turbo boost
  def calculateTurboBoostLocations(trackPieces: Array[TrackPiece], isLastLap: Boolean) = {

    // returns: list of tuples of straight section start track piece index + number of consequtive straight pieces.
    // tuples are ordered in descending order by segment length.
    @tailrec def calculateTurboBoostLocationsAcc(pieces: List[(TrackPiece, Int)], acc: List[(Int, Int)]): List[(Int, Int)] = {

      @tailrec def getNextStraight(currIdx: Int, startIdx: Int, len: Int): (Int, Int) = {
        val lastLap = isLastLap && (currIdx + 1 >= trackPieces.length)
        if (!trackPieces(currIdx).isBend && !lastLap)
          getNextStraight((currIdx + 1) % trackPieces.length, startIdx, len + 1)
        else {
          if (lastLap) (startIdx, len + 1)
          else (startIdx, len)
        }
      }

      if (pieces.isEmpty) {
        acc
      } else {
        val rest = if (pieces.head._1.isBend) pieces.dropWhile(p => p._1.isBend)
        else pieces
        if (!rest.isEmpty) {
          val s = getNextStraight(rest.head._2, rest.head._2, 0)
          calculateTurboBoostLocationsAcc(rest drop s._2, acc :+ s)
        } else {
          acc
        }
      }
    }

    val p = calculateTurboBoostLocationsAcc(trackPieces.toList.zipWithIndex, Nil)
    p.sortBy(i => i._2).reverse
  }

  def nextSwitchIndices(pieces: Array[TrackPiece]): Array[Int] = {
    val nextSwitchIndices = Array.ofDim[Int](pieces.length)

    val switchIndices = pieces.zipWithIndex.collect {
      case pair @ (p,i) if p.switch => i
    }

    for (i <- 0 until pieces.length) {
      val nextIdx = switchIndices.length match {
        case 0 => -1
        case 1 => switchIndices(0)
        case _ =>
          switchIndices.find(_ > i).
            getOrElse(switchIndices.find(_ < i).get)
      }
      nextSwitchIndices(i) = nextIdx
    }
    nextSwitchIndices
  }

}
