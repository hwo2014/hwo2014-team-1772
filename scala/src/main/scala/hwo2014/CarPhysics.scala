package hwo2014

import math.{abs, min,max,pow,log,sqrt}
import vector._

/**
 * Speed targets per each different bend.
 */
class BendSpeedTargets(targets: Array[Double], pieceCount: Int) {
  def this() = this(Array.ofDim(0), 0)

  private def index(pieceIndex: Int, laneIndex: Int) =
    pieceIndex * pieceCount + laneIndex

  def apply(pieceIndex: Int, laneIndex: Int): Double = {
    val i = index(pieceIndex, laneIndex)
    if (i >= targets.size) 0.0
    else targets(i)
  }

  def update(pieceIndex: Int, laneIndex: Int, value: Double): BendSpeedTargets = {
    val i = pieceIndex * pieceCount + laneIndex
    val newTargets = targets.padTo(i + 1, 0.0)
    newTargets(i) = value
    new BendSpeedTargets(newTargets, pieceCount)
  }
}

case class CarPhysics(
  /* drag is a constant for which
   * nextSpeed == currentSpeed * drag when throttle == 0 */
  drag: Double,
  /* Bend force is a contant for which it is safe to enter a bend at
   * the speed: maxSpeedEnteringBend = sqrt(maxBendForce * bendRadius) */
  maxBendForce: Double,
  /* The max carPosition.angle after which a car crashes.
   * This has never been anything else then 60*/
  maxAngle: Double,
  /* The maximum angle that our car has seen when we have reached the
   * maximum driftin angle at the speed calculated from
   * maxBendForce. I.e.
   * speed == sqrt(maxBendForce * bendRadius) && angleDelta == 0 */
   maxAngleSeen: Double,
  /* Car speed, i.e. distance between previous and current position */
  speed: Double,
  /* Change in car angle. Note that this is positive or negative
   * depending on the bend direction. Use bendDirection * angleDelta to
   * get absolute value. */
  angleDelta: Double,
  /* True if the speed was measured in a realiable way (same piece, same
   * lane, same lap). */
  speedIsReliable: Boolean,
  /* Acceleration when starting from speed 0.0 with throttle 1.0 */
  startAcceleration: Double,
  /* A set of samples (max 3) of previous accelerations. Each
   * value is Vector(oldSpeed, throttle, newSpeed) */
  accelerationVectors: List[Vector],
  /* A target bendForce for each individual bend. */
  bendForceTargets: BendSpeedTargets) {

  import CarPhysics._

  def defaultEstimateThrottle(currentSpeed: Double, targetSpeed: Double) =
    if (targetSpeed <= currentSpeed * drag) math.random * 0.01
    else (targetSpeed - (currentSpeed * drag)) / defaultMaxAcceleration

  def estimateThrottle(currentSpeed: Double, targetSpeed: Double) =
    if (accelerationVectors.isEmpty)
      defaultEstimateThrottle(currentSpeed, targetSpeed)
    else {
      // there is at least one value
      val vecs: List[Vector] =
        (accelerationBackupVectors(currentSpeed, drag) :::
           accelerationVectors).takeRight(3)

      calculateAccelerationPlane(vecs) map { plane =>
        solveY(plane, currentSpeed, targetSpeed)
      } getOrElse {
        CarPhysics.logger.debug(s"bad accelerationPlane with vectors $vecs")

        defaultEstimateThrottle(currentSpeed, targetSpeed)
      }
    }
}

object CarPhysics extends LoggerSupport {
  val defaultMaxAcceleration = 0.15
  val bendForceDelta = 0.05
  val epsilon = 0.01

  val empty: CarPhysics = CarPhysics(1.0, 0.45, 60, 20, 0.0,
                                     0.0, true, 0.0, Nil,
                                     new BendSpeedTargets)

  def updateOnCrash(carStats: CarStatistics): CarPhysics = {
    val old = carStats.state.carPhysics
    val previousCarPositions = carStats.carPositions
    val carId = carStats.carId
    val potentialPusher: Option[OpponentCarInfo] =
      carStats.opponentCarInfos.
        filter(ci => ci.isBehindUs && ci.isSameLaneAsUs).
        filter(_.distance < min(old.speed, carStats.carDimensions.length)).
        sortBy(_.distance).headOption

    logger.info("On crash, opponents were as follows:\n" + carStats.opponentCarInfos.mkString("\n"))

    potentialPusher.map { pusher =>
      logger.info(s"We were probably pushed out by ${pusher.carId}, distance ${pusher.distance} behind us")
      old.copy(speed = 0, speedIsReliable = true)
    }.getOrElse {
      // we crashed so we exceeded maxBendForce and maxAngle, calculate
      val carPosition = previousCarPositions(carId)
      val maxAngle = 60 //abs(carPosition.angle + (old.angleDelta / 2))
      val maxBendForce =
        max(0.4, // sane default
            old.maxBendForce - (bendForceDelta * abs(old.angleDelta + epsilon)))

      logger.info(f"New maxAngle $maxAngle%.2f, new maxBendForce $maxBendForce%.3f (was ${old.maxBendForce}%.3f)")

      old.copy(maxAngle = min(old.maxAngle, maxAngle),
               maxBendForce = min(old.maxBendForce, maxBendForce),
               speed = 0,
               speedIsReliable = true,
               accelerationVectors = Nil,
               maxAngleSeen = old.maxAngleSeen * pow(old.maxBendForce / maxBendForce, 2))
    }

  }
  def updateOnDrive(old: CarPhysics, track: TrackStructure, carId: CarId,
                    previousCarPositions: CarPositions,
                    newCarPositions: CarPositions,
                    previousReaction: AgentReaction): CarPhysics = {
    val previousCarPosition = previousCarPositions(carId)
    val newCarPosition = newCarPositions(carId)
    val newSpeedOption = 
      distanceDelta(previousCarPosition, newCarPosition, track) flatMap { dd =>
        speed(dd, previousCarPositions.gameTick,
            newCarPositions.gameTick)
      }

    newSpeedOption.foreach { newSpeed =>
      if (old.drag != 1.0 && newSpeed*(1+epsilon*2) < old.speed * old.drag) {
        logger.warn(
          s"newSpeed: $newSpeed (piece ${newCarPosition.piece}), " +
          s"old.speed: ${old.speed} (piece ${previousCarPosition.piece}), " +
          s"old.drag = ${old.drag}"
        )
      }
    }

    val newStartAcceleration = newSpeedOption match {
      case Some(speed) if (old.speed == 0 && old.speedIsReliable) => speed
      case _ => old.startAcceleration
    }

    val newAngleDelta =
      newCarPositions(carId).angle - previousCarPositions(carId).angle

    val newDrag =
      newSpeedOption flatMap { s =>
        dragUpdate(old.speed, s, previousReaction, old.speedIsReliable) map { d =>
          if (old.drag == 1.0) d else (d + (old.drag * 10)) / 11
        }
      } getOrElse old.drag

    val newAccelerationVectors =
      accelerationVectorsUpdate(old.accelerationVectors,
                                old.speed,
                                old.speedIsReliable,
                                previousReaction,
                                newSpeedOption,
                                newDrag)

    val newMaxBendForce =
      maxBendForceUpdate(old, track, previousCarPosition, newCarPosition,
                         newAngleDelta)

    val newSpeed = newSpeedOption getOrElse old.speed

    CarPhysics(newDrag,
               newMaxBendForce,
               max(old.maxAngle, newCarPosition.angle),
               if (newMaxBendForce > old.maxBendForce)
                 (old.maxAngleSeen + 3 * max(old.maxAngleSeen, previousCarPosition.angle)) / 4
               else old.maxAngleSeen,
               newSpeed,
               newAngleDelta,
               newSpeedOption.isDefined,
               newStartAcceleration,
               newAccelerationVectors,
               old.bendForceTargets)
  }

  def withinEpsilon(x: Double, y: Double): Boolean = abs(x - y) <= (max(x, y) * epsilon)

  def accelerationBackupVectors(currentSpeed: Double, drag: Double) =
    List(Vector(0.0, 1.0, 0.2),
         Vector(currentSpeed, 0.0, currentSpeed * drag))

  def calculateAccelerationPlane(vecs: List[Vector]): Option[Plane] = {
    val accelerationPlane = plane(vecs(0),
                                  vecs(1),
                                  vecs(2))

    if (solveY(accelerationPlane, 6, 6 + epsilon) <
          solveY(accelerationPlane, 6, 6 + 2 * epsilon))
      Some(accelerationPlane)
    else
      None
  }

  def accelerationVectorsUpdate(oldAccelerationVectors: List[Vector],
                                oldSpeed: Double,
                                oldSpeedIsReliable: Boolean,
                                previousReaction: AgentReaction,
                                newSpeedOption: Option[Double],
                                drag: Double): List[Vector] = {
    newSpeedOption map { speed =>
      previousReaction match {
        case Throttle(throttle) if oldSpeedIsReliable && oldSpeed > 0.0 && !withinEpsilon(oldSpeed, speed) && throttle > 0.0 =>
          val v = Vector(oldSpeed, throttle, speed)
          val (tooClose, other) =
            oldAccelerationVectors.partition(other =>
              withinEpsilon(other.x, v.x) || withinEpsilon(other.y, v.y)
            )

          // if two were dropped, don't update at all (otherwise our
          // crossproduct will be Infinite)
          if (tooClose.size > 1) {
            oldAccelerationVectors
          } else {
            val vecs = v :: (if (other.size < 3) other else other.take(2))
            if (vecs.size < 3) vecs
            else if (calculateAccelerationPlane(vecs).isDefined) vecs
            else oldAccelerationVectors.take(2) // vecs resulted in bad values
          }
        case _ => oldAccelerationVectors
      }
    } getOrElse (oldAccelerationVectors)
  }

  def maxBendForceUpdate(old: CarPhysics, track: TrackStructure,
                         previousCarPosition: CarPosition,
                         newCarPosition: CarPosition,
                         angleDelta: Double): Double = {
    val previousIndex = previousCarPosition.piece.index
    val previousLaneIndex = previousCarPosition.piece.lane.endIndex
    val currentIndex = newCarPosition.piece.index
    val currentLaneIndex = newCarPosition.piece.lane.startIndex
    val bendDirection = track.bendDirection(previousIndex)
    val bendRadius = track.bendRadius(previousIndex, previousLaneIndex)
    val maxAngleDiff = old.maxAngle - (bendDirection * previousCarPosition.angle)
    val oldTargetSpeed = sqrt(old.maxBendForce * bendRadius)
    val currentRadius = track.bendRadius(currentIndex, currentLaneIndex)
    if (bendDirection != 0 && withinEpsilon(oldTargetSpeed, old.speed) &&
          maxAngleDiff == old.maxAngle && (bendDirection * angleDelta) == 0 &&
          old.maxAngleSeen == 0) {
      logger.debug(f"${old.maxBendForce}%.3f is way too low maxBendForce")

      old.maxBendForce + bendForceDelta
    } else if (bendDirection != 0 &&
                 bendRadius != currentRadius &&
                 previousCarPosition.angle >= old.maxAngleSeen &&
                 maxAngleDiff > 0 && old.speedIsReliable &&
                 maxAngleDiff < old.maxAngle &&
                 withinEpsilon(oldTargetSpeed, old.speed)) {

      val newMaxBendForce =
        old.maxBendForce + (bendForceDelta * pow(maxAngleDiff / old.maxAngle, 2))

      logger.debug(f"$newMaxBendForce%.3f, $maxAngleDiff%.1f (${previousCarPosition.angle}%.1f > ${old.maxAngleSeen}%.2f in bend $previousIndex: $bendDirection $bendRadius )")

      newMaxBendForce
    } else old.maxBendForce
  }

  /** Distance travelled from previous update, if it can be reliably
    * measured.  None if the car has changed lanes. */
  def distanceDelta(previousCarPosition: CarPosition,
                    newCarPosition: CarPosition,
                    track: TrackStructure): Option[Double] = {

    val newLane = newCarPosition.piece.lane
    val previousPieceIndex = previousCarPosition.piece.index
    val newPieceIndex = newCarPosition.piece.index

    if (previousCarPosition.piece.lane == newLane) {
      val isOneTravelledPiece = (newPieceIndex - previousPieceIndex == 1) ||
        (previousPieceIndex == (track.pieces.length-1) && newPieceIndex == 0)
      if (previousPieceIndex == newPieceIndex) {
        Some(newCarPosition.piece.distance - previousCarPosition.piece.distance)
      } else if (isOneTravelledPiece) {
        val previousPieceDistance = track.pieceLength(previousPieceIndex, newLane.endIndex) - previousCarPosition.piece.distance
        Some(previousPieceDistance + newCarPosition.piece.distance)
      } else {
        None
      }
    } else {
      None
    }

    /*
    if (previousCarPosition.piece.index == newCarPosition.piece.index &&
      previousCarPosition.lap == newCarPosition.lap &&
      previousCarPosition.piece.lane.endIndex == newCarPosition.piece.lane.endIndex)
      Some(newCarPosition.piece.distance - previousCarPosition.piece.distance)
    else None
    */
  }

  /** Speed in distanceDelta per tick. Only reliable if one tick since previous tick. */
  def speed(distanceDelta: Double, oldTick: Long, newTick: Long): Option[Double] =
    if (newTick - oldTick == 1) Some(distanceDelta)
    else None

  /** Drag constant [0 - 1.0]. Can be measured only if Throttle = 0. 1.0 means no drag */
  def dragUpdate(oldSpeed: Double, currentSpeed: Double,
                 previousReaction: AgentReaction, speedIsReliable: Boolean):
      Option[Double] =
    previousReaction match {
      case Throttle(0.0) if speedIsReliable =>
        Some(1 - ((oldSpeed - currentSpeed) / oldSpeed))
      case _ => None
    }
}
