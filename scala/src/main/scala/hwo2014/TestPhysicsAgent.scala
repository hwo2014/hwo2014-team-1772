package hwo2014

object TestPhysicsAgent {
  def react(carPhysics: CarPhysics): AgentReaction =
    if (carPhysics.accelerationVectors.size < 3)
      // test acceleration by varying the throttle
      Throttle((1.0 - CarPhysics.epsilon) / (3 - carPhysics.accelerationVectors.size))
    else if (carPhysics.drag == 1.0 && carPhysics.speedIsReliable && carPhysics.speed > 3.0)
      // test drag by breaking
      Throttle(0.0)
    else NOP
}
