package hwo2014

import org.json4s.JValue

object AlphaBotApp extends App {
  import org.rogach.scallop.ScallopConf
  import org.json4s.JsonDSL._

  // ---------- START EXECUTION ----------

  val params = new Params(args) // validate params
  val (host, port, joinMessage, exitOnCrash, logToFile) = params.trail.get match {
    case s @ Some(List(host, port, botName, botKey)) =>
      (host, port.toInt, defaultJoin(botName, botKey), false, false)
    case _ =>
      val host = params.host.get.get
      val port = params.port.get.get

      val validJoins = Set("joinRace","createRace")
      val maybeJoin = params.joinType.get.filter(validJoins)
      val joinMessage = maybeJoin.map { joinType =>
        ("msgType" -> joinType) ~ ("data" ->
          ("botId" -> ("name" -> params.botName.get.get) ~ ("key" -> AlphaBot.BotKey)) ~
          ("carCount" -> params.cars.get.get) ~
          ("trackName" -> params.track.get) ~
          ("password" -> params.password.get)
        )
      }.getOrElse(defaultJoin())
      (host, port, joinMessage, params.exitOnCrash.get.get, params.logToFile.get.get)
  }
  val agent = new MasterAgent(exitOnCrash) // DefaultAgent
  val bot = new IO(host, port, agent, logToFile)
  bot.start(joinMessage)

  // ---------- END EXECUTION ----------


  class Params(arguments: Seq[String]) extends ScallopConf(arguments) {
    import org.rogach.scallop._

    val host = opt[String](default = Some("testserver.helloworldopen.com"),
      descr = "hakkinen.helloworldopen.com (R2), senna.helloworldopen.com (R1), webber.helloworldopen.com(R3)")
    val port = opt[Int](noshort = true, default = Some(8091))

    val botName = opt[String](default = Some(AlphaBot.BotName))
    val joinType = opt[String](descr = "joinRace or createRace")
    val track = opt[String](descr = "track name, e.g. hockenheim")
    val password = opt[String]()
    val cars = opt[Int](descr = "car count", validate = _ > 0, default = Some(1))
    val exitOnCrash = opt[Boolean](default = Some(false))
    val logToFile = opt[Boolean](default = Some(false))

    val trail = trailArg[List[String]]("host port botName botKey", required = false)
  }

  private def defaultJoin(botName: String = AlphaBot.BotName,
                          botKey: String = AlphaBot.BotKey): JValue =
    ("msgType", "join") ~ ("data" ->
      ("name", botName) ~ ("key", botKey)
    )
}

object AlphaBot {
  val BotKey = "N4Czau5JiidbSQ"
  val BotName = "Alpha Dogs"
  val CharSet = "UTF-8"
}
