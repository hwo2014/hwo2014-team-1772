package hwo2014

import math.{abs,min,max,log,pow}

/** A simple turbo agent that tries to find a straight where to use
  * the turbo.
  *
  * TODO: this should be refactored so that it uses the
  * turbo on the longest straight before finish.
  */
object TurboAgent extends LoggerSupport {

  def react(turboAvailable: Option[TurboAvailable], carStatistics: CarStatistics): AgentReaction = {
    val carId = carStatistics.carId
    val carPosition = carStatistics.carPositions(carId)
    val currentIndex = carPosition.piece.index
    val track = carStatistics.state.track
    val useTurbo = !track.isBend(currentIndex) && turboAvailable.map { turbo =>
      val distanceToBend = (
        track.distanceToBend(currentIndex, carPosition.piece.lane.endIndex) -
          carPosition.piece.distance
      )
      val currentSpeed = carStatistics.state.carPhysics.speed
      val timeToBend = distanceToBend / currentSpeed

      // determine best locations for turbo boost
      val isLastLap = carStatistics.state.gameInfo.raceSession match {
        case MainRaceSession(laps: Int, maxLapTimeMs: Long, _) => carPosition.lap == laps
        case _ => false
      }
      val boostLocs = track.turboBoostLocations(isLastLap)

      (timeToBend > turbo.durationTicks) &&
        carStatistics.opponentCarInfos.exists { ci =>
          ci.isFrontOfUs && ci.distance < carStatistics.carDimensions.length * 3
        } == false && {
          if (boostLocs.nonEmpty) {
            val bestStraightLength = boostLocs.head._2
            boostLocs.takeWhile(_._2 >= bestStraightLength).exists(_._1 == carPosition.piece.index)
          } else false
        }
    }.getOrElse(false)

    if (useTurbo) {
      logger.debug(s"** turbo boost (at piece idx: ${carPosition.piece.index}}) **")
      Turbo.withRandomMessage
    } else NOP
  }



}
