package hwo2014

import math.{Pi,abs,min,max,log,pow,sqrt,tanh}

/** An agent that tries to maintain the bendForce at maxBendForce */
object DriftAgent extends LoggerSupport {
  val maxSpeedDiff = 0.10
  val epsilon = 0.01
  val x = 37
  val f37 = f(0)
  val fDelta = f(1) - f37
  def f(y: Double): Double = pow(tanh((x+y)*Pi/180),2)
  def g(delta: Double, ticks: Double): Double = {
    val ft = f(ticks)
    val r = (ft - f37) * (delta / fDelta)
//    logger.debug(f"f($ticks%.1f) $ft%.3f, ($ft%.3f - $f37%.3f) * $delta%.3f / $fDelta%.3f = $r%.3f ")
    r
  }


  def react(track: TrackStructure,
            previousCarPhysics: CarPhysics,
            carPhysics: CarPhysics,
            carPosition: CarPosition,
            previousReaction: AgentReaction): AgentReaction = {
    val currentIndex = carPosition.piece.index
    val bendDirection = track.bendDirection(currentIndex)
    if (bendDirection == 0) Throttle(1.0)
    else {
      val currentSpeed = carPhysics.speed
      val bendEnd = track.bendEndIndex(currentIndex)
      val distanceToBendEnd = track.carDistanceToPiece(carPosition, bendEnd)
      val ticksToBendEnd = (distanceToBendEnd - (carPhysics.speed / 2)) / carPhysics.speed
      val estimatedAngle = carPosition.angle + g(carPhysics.angleDelta, ticksToBendEnd)
      val normalizedAngle = bendDirection * carPosition.angle
      val targetSpeed: Double =
        if (normalizedAngle > (carPhysics.maxAngle - 1) ||
              (normalizedAngle > 35.0 &&
                 (bendDirection * estimatedAngle) > carPhysics.maxAngle)) {
          // BRACE BRACE BRACE
          0.0
        } else if ((bendDirection * carPhysics.angleDelta) <=
              (bendDirection * previousCarPhysics.angleDelta) &&
              (bendDirection * estimatedAngle) < carPhysics.maxAngle) {
          currentSpeed * (1 + CarPhysics.bendForceDelta)
        } else {
          val laneIndex = carPosition.piece.lane.endIndex
          val bendRadius = track.bendRadius(currentIndex, laneIndex)
          sqrt(carPhysics.maxBendForce * bendRadius)
        } 

      val throttle = carPhysics.estimateThrottle(currentSpeed, targetSpeed)

//      logger.debug(f"$currentSpeed%.3f/$targetSpeed%.3f ${carPosition.angle}%.2f, estimated: $estimatedAngle%.2f")

      Throttle(throttle)
    }
  }
}
