package hwo2014

object RouteAgent extends LoggerSupport {

  def react(carStats: CarStatistics): Option[(AgentReaction, Double)] = {
    val carPos = carStats.carPositions(carStats.carId)
    val optimalLaneReaction: Option[(SwitchLane, Double)] = carStats.state.track.switchLaneReaction(carPos)
    val opponentReaction: Option[SwitchLane] = calculateOpponentReaction(carStats, optimalLaneReaction.map(_._1))

    val combinedRouteReaction = opponentReaction.map(s => s -> 0.0).orElse(optimalLaneReaction)
    combinedRouteReaction.flatMap {
      case r @ (newSwitch, distance) =>
        carStats.state.pendingSwitch match {
          case Some(pending) if pending == newSwitch =>
            None
          case _ =>
            Some(r)
        }
    }
  }

  private def calculateOpponentReaction(carStats: CarStatistics, optimalLaneSwitch: Option[SwitchLane]): Option[SwitchLane] = {
    val tick = carStats.gameTick
    val carId = carStats.carId
    val carPositions = carStats.carPositions
    val ourCarPosition = carPositions(carId)
    val currentPieceIndex = ourCarPosition.piece.index
    val track = carStats.state.track
    val physics = carStats.state.carPhysics
    val currentLaneIndex = ourCarPosition.piece.lane.endIndex

    val distanceToSwitch = track.carDistanceToSwitch(ourCarPosition)
    val ourCarDimensions = carStats.cars.find(_.id == carId).get.dimensions

    def isSafeBendRadius(bendIndex: Int): Boolean =
      math.abs(track.bendRadius(bendIndex, currentLaneIndex)) > ourCarDimensions.length*3

    // Rationale: no point calculating far from switch, because other car positions may change radically
    val isDecisionTime = (distanceToSwitch - physics.speed*1.2) < 0
    lazy val isSafeBendWise: Boolean = {
      val currentPositionSafe =
        if (!track.isBend(currentPieceIndex)) true else {
          isSafeBendRadius(currentPieceIndex)
        }
      currentPositionSafe && {
        val closeToBend = track.distanceToBend(currentPieceIndex, currentLaneIndex) < ourCarDimensions.length*4
        if (closeToBend)
          isSafeBendRadius(track.nextBendIndices(currentPieceIndex))
        else
          true
      }
    }
    if (carPositions.cars.length > 1 && track.lanes.length > 1 && isDecisionTime && isSafeBendWise) {
      val others = carStats.opponentCarInfos
      val potentialBlocker =
        others.filterNot(ci => ci.isBehindUs ||
                               ci.distance > ourCarDimensions.length*2 ||
                               ci.speed.map(_ > physics.speed).getOrElse(true)).
          sortBy(_.distance).headOption

      logger.info(s"Potential blocker ahead: $potentialBlocker (tick $tick)")

      potentialBlocker.flatMap { blocker =>
        import carStats.state.track.resultingLaneIndex
        def isFreeLane(laneIndex: Int): Boolean = {
          others.exists { case other =>
            other.carPos.piece.lane.endIndex == laneIndex && other.distance < (ourCarDimensions.width * 1.15)
          } == false
        }

        val blockerLaneIndex = blocker.carPos.piece.lane.endIndex
        val ourDesiredLaneIndex =
          optimalLaneSwitch.map(resultingLaneIndex(currentLaneIndex,_)).getOrElse(currentLaneIndex)

        val result = if (blockerLaneIndex == ourDesiredLaneIndex) {
          if (isFreeLane(resultingLaneIndex(currentLaneIndex, SwitchLaneLeft))) {
            Some(SwitchLaneLeft)
          } else if (isFreeLane(resultingLaneIndex(currentLaneIndex, SwitchLaneRight))) {
            Some(SwitchLaneRight)
          } else {
            None
          }
        } else {
          None
        }
        logger.info(s"We choose to $result given blocker $blocker, and optimalLane $optimalLaneSwitch")
        result
      }
    } else {
      None
    }
  }


}

