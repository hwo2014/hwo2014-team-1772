package hwo2014

import org.specs2.mutable._
import java.io.File
import scala.io.Source
import org.json4s.JValue
import org.json4s.jackson.JsonMethods.parse

class JsonSupportSpec extends Specification {
  "JsonSupport" should {

    val gameInitFile = file("/test_gameInit.json")
    s"parseGameInfo from ${gameInitFile.getName}" in {
      val gameInfo = JsonSupport.parseGameInfo(fileToJson(gameInitFile))
      gameInfo.gameId === "foobar"
      gameInfo.track.startingPoint === StartingPoint(-300.0, -44.0, 90.0)
      gameInfo.track.id === "keimola"
      val pieces = gameInfo.track.pieces
      pieces.head === Straight(100.0, false)
      pieces(3) === Straight(100.0, true)
      pieces(4) === Bend(100.0, 45.0, false)
    }

    val carPositionsFile = file("/test_carPositions.json")
    s"parseCarPositions from ${carPositionsFile.getName}" in {
      val CarPositions(tick, cars) = JsonSupport.parseCarPositions(fileToJson(carPositionsFile))
      tick === 806L
      cars === List(
        CarPosition(
          CarId("Alpha Dogs", "red"),
          0.0,
          PiecePosition(2, 85.21257554177409, LanePosition(2,3)),
          1
        )
      )
    }

    val gameEndFile = file("/test_gameEnd.json")
    s"parseGameResults from ${gameEndFile.getName}" in {
      val GameResults(results) = JsonSupport.parseGameResults(fileToJson(gameEndFile))
      results === List(
        CarResult(
          CarId("Alpha Dogs", "red"),
          Some(TotalResult(3, 1636, 27267)),
          Some(BestLap(1, 532, 8866))
        )
      )
    }

    val turboFile = file("/test_turboAvailable.json")
    s"parseTurboAvailable from ${turboFile.getName}" in {
      JsonSupport.parseTurboAvailable(fileToJson(turboFile)) ===
        TurboAvailable(500, 30, 3.0)
    }

  }

  private def fileToJson(file: File): JValue =
    parse(Source.fromFile(file).getLines.mkString)

  private def file(f: String) =
    new File(this.getClass.getResource(f).getFile)

}
