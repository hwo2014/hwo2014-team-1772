package hwo2014

import org.specs2.mutable._
import java.io.File
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.io.Source

class TrackStructureSpec extends Specification {

  def bendLength(angle: Double, radius: Int) =
    math.Pi * radius * (angle/180.0)

  def beVeryCloseTo(d: => Double) = beCloseTo(d +/- 0.01)

  val game = file("/test_route_planning.json")
  val gameInfo = JsonSupport.parseGameInfo(fileToJson(file("/test_route_planning.json")))
  val ts = TrackStructure(gameInfo.track)

  "TrackStructure.distanceToPiece" should {
    "handle pieces 3..9 of keimola" in {
      ts.distanceToPiece(3, 10, 0) must
        beVeryCloseTo(100.0 + bendLength(45.0, 100 + 10)*4 + bendLength(22.5, 200 + 10) + 100)
      ts.distanceToPiece(3, 10, 1) must
        beVeryCloseTo(100.0 + bendLength(45.0, 100 - 10)*4 + bendLength(22.5, 200 - 10) + 100)
      /*
      { // 3
          "length": 100.0,
          "switch": true
      },
      { // 4
          "angle": 45.0,
          "radius": 100
      },
      { // 5
          "angle": 45.0,
          "radius": 100
      },
      { // 6
          "angle": 45.0,
          "radius": 100
      },
      { // 7
          "angle": 45.0,
          "radius": 100
      },
      { // 8
          "angle": 22.5,
          "radius": 200,
          "switch": true
      },
      { // 9
          "length": 100.0
      },
      */
    }

    "handle pieces 11..15 of keimola" in {
      ts.distanceToPiece(11, 16, 0) must beVeryCloseTo(bendLength(22.5,200-10) + 100*2 + bendLength(45, 100-10)*2)
      ts.distanceToPiece(11, 16, 1) must beVeryCloseTo(bendLength(22.5,200+10) + 100*2 + bendLength(45, 100+10)*2)
      /*
      { // 11
          "angle": -22.5,
          "radius": 200
      },
      { // 12
          "length": 100.0
      },
      { // 13
          "length": 100.0,
          "switch": true
      },
      { // 14
          "angle": -45.0,
          "radius": 100
      },
      { // 15
          "angle": -45.0,
          "radius": 100
      },
       */
    }

  }

  "TrackStructure.switchLaneForNextBendByPieceAndLaneIndex" should {
    "give valid results" in {
      ts.switchLaneReactionByPieceAndLaneIndex(0, 0) === Some(SwitchLaneRight -> 300.0)
      ts.switchLaneReactionByPieceAndLaneIndex(0, 1) === None
      ts.switchLaneReactionByPieceAndLaneIndex(2, 0, 55.0) === Some(SwitchLaneRight -> 45.0)

      ts.switchLaneReactionByPieceAndLaneIndex(3, 0, 10.0) === None
      ts.switchLaneReactionByPieceAndLaneIndex(3, 1, 10.0).get._1 === SwitchLaneLeft

      ts.switchLaneReactionByPieceAndLaneIndex(38, 0) === Some(SwitchLaneRight -> 490.0)
      ts.switchLaneReactionByPieceAndLaneIndex(38, 1) === None
    }
  }

  "TrackStructure.calculateTurboBoostLocations" should {
    "calculate expected boost locations" in {
      TrackStructure.calculateTurboBoostLocations(ts.pieces, false) === List((35,9), (0,4), (12,2), (9,2), (28,1), (25,1), (18,1))
      TrackStructure.calculateTurboBoostLocations(ts.pieces, true) === List((35,5), (0,4), (12,2), (9,2), (28,1), (25,1), (18,1))
    }
  }

  private def fileToJson(file: File): JValue =
    parse(Source.fromFile(file).getLines.mkString)

  private def file(f: String) =
    new File(this.getClass.getResource(f).getFile)

}
