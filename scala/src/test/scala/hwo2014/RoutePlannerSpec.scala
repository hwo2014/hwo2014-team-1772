package hwo2014

import org.specs2.mutable._
import java.io.File
import scala.io.Source
import org.json4s.JValue
import org.json4s.jackson.JsonMethods.parse

class RoutePlannerSpec extends Specification {
  "RoutePlanner" should {

    val game = file("/test_route_planning.json")
    s"test SimpleRoutePlanner ${game.getName}" in {
      val gameInfo = JsonSupport.parseGameInfo(fileToJson(game))
      gameInfo.track.pieces.length === 40
      SimpleRoutePlanner.planRoute(gameInfo.track) === List(0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    }

    s"test DynamicProgrammingPlanner @ keimola from ${game.getName}" in {
      val gameInfo = JsonSupport.parseGameInfo(fileToJson(game))
      val p = DynamicProgrammingPlanner.calculatePolicy(gameInfo.track)
      p === Array(
        List(Ping, Ping),
        List(Ping, Ping),
        List(SwitchLaneRight, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, SwitchLaneLeft),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, SwitchLaneLeft),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(SwitchLaneRight, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(SwitchLaneRight, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(SwitchLaneRight, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(SwitchLaneRight, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping)
      )
    }

    val g2 = file("/test_route_planning_germany.json")
    s"test DynamicProgrammingPlanner @ germany ${g2.getName}" in {
      val gameInfo = JsonSupport.parseGameInfo(fileToJson(g2))
      val p = DynamicProgrammingPlanner.calculatePolicy(gameInfo.track)
      p === Array(
        List(Ping, SwitchLaneLeft),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(SwitchLaneRight, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(SwitchLaneRight, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, SwitchLaneLeft),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(SwitchLaneRight, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(SwitchLaneRight, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(SwitchLaneRight, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping),
        List(Ping, Ping)
      )
    }

  }

  private def fileToJson(file: File): JValue =
    parse(Source.fromFile(file).getLines.mkString)

  private def file(f: String) =
    new File(this.getClass.getResource(f).getFile)

}
