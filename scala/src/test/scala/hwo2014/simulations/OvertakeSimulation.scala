package hwo2014.simulations

import hwo2014._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import hwo2014.GameInfo
import hwo2014.CarId

object OvertakeSimulation extends App with Simulation with LoggerSupport {

  val Password = uuid

  class SlowAgent extends MasterAgent(false) {

    var hasSwitchedLanes = false

    /*
    override def drive(state: MasterAgent.State, carPositions: CarPositions) = {
      if (hasSwitchedLanes) {
        state -> Throttle(0.5)
      } else {
        hasSwitchedLanes = true
        val carPos = carPositions(state.carId)
        if (carPos.piece.lane.endIndex == 0)
          state -> SwitchLaneRight
        else
          state -> SwitchLaneLeft
      }
    }
    */
    override def drive(state: MasterAgent.State, carPositions: CarPositions) = {
      val (newState, reaction) = super.drive(state, carPositions)
      newState -> (if (reaction.isInstanceOf[Throttle1]) Throttle(0.5) else reaction)
    }
  }

  class DelayedMasterAgent extends MasterAgent(false) {
    override def drive(state: MasterAgent.State, carPositions: CarPositions) = {
      val (newState, reaction) = super.drive(state, carPositions)
      newState -> (if (carPositions.gameTick < 70) NOP else reaction)
    }
  }

  Future {
    io(new SlowAgent, false).start(joinMessage("createRace", "Slow dude", Password))
  }
  Thread.sleep(1000)
  Future {
    io(new DelayedMasterAgent, true).start(joinMessage("joinRace", "Overtaker", Password))
  }

  while(true) {
    Thread.sleep(2000)
  }

}
