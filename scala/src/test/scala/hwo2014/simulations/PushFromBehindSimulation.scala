package hwo2014.simulations

/*
import hwo2014._
import hwo2014.GameInfo
import hwo2014.CarId

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object PushFromBehindSimulation extends App with Simulation with LoggerSupport {

  val Password = uuid

  class Chaser extends Agent[Unit] {
    type State = Unit
    var hasSwitchedLane = false

    def initialize(carId: CarId, gameInfo: GameInfo) = ()

    def drive(gameState: GameState[State]) = {
      val tick = gameState.gameTick
      if (tick > 600) {
        logger.info(s"Stopping at tick $tick")
        sys.exit(0)
      }

      val gameInfo = gameState.gameInfo
      val track = gameInfo.track
      val ourPosition = gameState.carPosition

      val ourPieceIdx = ourPosition.piece.index
      val ourLaneIdx = ourPosition.piece.lane.endIndex
      val currentPiece = track.pieces(ourPieceIdx)

      val victimCar = gameState.carPositions.cars.filterNot(_ == ourPosition).head
      val victimDistance = position(track.pieceStartPositions, victimCar)
      if (victimDistance > 650) {
        if (currentPiece.switch && !hasSwitchedLane) {
          hasSwitchedLane = true
          val lanes = gameState.gameInfo.track.lanes
          val switchReaction = lanes.find(_.index == ourLaneIdx-1).map(_ => SwitchLaneLeft).getOrElse(SwitchLaneRight)
          logger.debug(s"$switchReaction")
          ((), switchReaction)
        } else {
          val speed = if (ourPosition.piece.index > 6) 1.0 else 0.6
          ((), Throttle(speed))
        }
      } else {
        () -> NOP
      }
    }
  }

  Future {
    io(staticSpeedAgent(0.3), false).start(joinMessage("createRace", "Victim", Password))
  }
  Thread.sleep(1000)
  Future {
    io(new Chaser, true).start(joinMessage("joinRace", "Chaser", Password))
    sys.exit(0)
  }

  while(true) {
    Thread.sleep(2000)
  }

}
 */
