package hwo2014.simulations

import hwo2014._
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._
import java.util.UUID
import hwo2014.GameInfo
import hwo2014.CarId

trait Simulation {

  def uuid = UUID.randomUUID.toString

  def joinMessage(msgType: String, botName: String, password: String,
           trackName: String = "keimola", carCount: Int = 2): JValue =
    ("msgType" -> msgType) ~ ("data" ->
      ("botId" -> ("name" -> botName) ~ ("key" -> AlphaBot.BotKey)) ~
      ("carCount" -> carCount) ~
      ("trackName" -> trackName) ~
      ("password" -> password)
    )

  def io(agent: MasterAgent, log: Boolean) =
    new IO("hakkinen.helloworldopen.com", 8091, agent, log)

}

