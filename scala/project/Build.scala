import sbt._
import Keys._
import sbtassembly.Plugin.AssemblyKeys._
import sbtassembly.Plugin.MergeStrategy

object AlphaDogsBuild extends Build {

  import Dependencies._

  lazy val IntegrationTestConfig = config("integration") extend(Test)
  def isIntegrationTest(name: String): Boolean = name.endsWith("IntegrationTest")
  def isUnitTest(name: String): Boolean = !isIntegrationTest(name)


  lazy val root = Project(
    id = "hwo2014bot",
    base = file("."),
    settings = Defaults.defaultSettings ++ Seq(
      organization := "hwo2014bot",
      version      := "0.1-SNAPSHOT",
      // scalaVersion := "2.10.4",
      offline := true,
      resolvers ++= ExtraResolvers,
      // Let's not use dependencies, but load the manually from `lib` dir
      //libraryDependencies ++= ProjectLibs,
      //libraryDependencies ++= CiOfflineLibs,
      unmanagedJars in Test ++= (new java.io.File(baseDirectory.value, "lib_test") ** "*.jar").classpath,
      testOptions in Test += Tests.Filter(isUnitTest),
      testOptions in IntegrationTestConfig ~= (_.filterNot(_.isInstanceOf[Tests.Filter]) :+ Tests.Filter(isIntegrationTest))
    ) ++ sbtassembly.Plugin.assemblySettings ++ Seq(
      mainClass in assembly := Some("hwo2014.AlphaBotApp"),
      jarName in assembly := "hwo2014bot.jar"/*,
      mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
        {
          case "library.properties" => MergeStrategy.first
          case x if x.startsWith("scala/") => MergeStrategy.first
          case x => old(x)
        }
      }
      */
    ) // ++ com.github.retronym.SbtOneJar.oneJarSettings
  ).configs(IntegrationTestConfig).settings(
    inConfig(IntegrationTestConfig)(Defaults.testTasks) : _*
  )

}

object Dependencies {

  val ExtraResolvers = Seq(
    "sonatype-public" at "https://oss.sonatype.org/content/groups/public"
  )

  private val scalaLangExclusions = ExclusionRule(organization = "org.scala-lang")

  val CiOfflineLibs = Seq(
    "org.specs2"         %% "specs2"               % "2.3.3" % "test"
  )

  val ProjectLibs = Seq(
    "com.typesafe"       %% "scalalogging-slf4j"   % "1.0.1" excludeAll(scalaLangExclusions),
    "org.json4s"         %% "json4s-jackson"       % "3.2.8",
    "com.typesafe"       %  "config"               % "1.0.2",
    "ch.qos.logback"     %  "logback-classic"      % "1.0.13",
    "org.rogach"         %% "scallop"              % "0.9.5",
    "joda-time"          %  "joda-time"            % "2.3",
    "org.joda"           %  "joda-convert"         % "1.6"
  ) ++ CiOfflineLibs

}
